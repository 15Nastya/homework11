import { useState } from 'react';
import './List.scss';  
import ListItem from '../ListItem/ListItem.js';

function List({ list = []}) {
    
    return (
        <div className={'List'}>
           
           {
            list.map((item) => {
                return <ListItem key={item.id} img={item.image} title={item.title} price={item.price}></ListItem> 
            })
           }
        </div>
    )
}

export default List;