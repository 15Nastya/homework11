import { useEffect, useState } from 'react';
import './App.scss';
import List from './components/List/List.js';
  

function App() {
  const [ productList, setProductList ] = useState([]);
  const [ searchProductlist, setSearchProductList ] = useState([]);
  const [ isLoaded, setIsLoaded ] = useState (false);
  const [ searchString, setSearchString ] = useState('');
 


  useEffect(() => {
    fetch ('https://fakestoreapi.com/products/')
      .then((response) => response.json())
      .then((result) => {
        setProductList(result);
        setSearchProductList(result);
        setIsLoaded(true);
      });
  }, []);

  useEffect(() => {
      const searchProductList = productList.filter((product) => {
        console.log(product);
        const productTitle = String(product.title) || '';
        return productTitle.includes(searchString);
      });

      setSearchProductList(searchProductList);
  }, [ searchString ]);
  
  return (
    <div className="App">
      <input 
      value={searchString} 
      placeholder={'Поиск по товарам'} 
      onInput={(event) => setSearchString(event.target.value)}
      ></input>
      {
        isLoaded && <List list={searchProductlist}></List>
      }
      {
        !isLoaded && <span>Loading...</span>
      }
        
    </div>  
  );
}

export default App;
